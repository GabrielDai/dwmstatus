# Dwmstatus fork

Original repo: git://git.suckless.org/dwmstatus

## Install

sudo make PREFIX=/usr install

And don't forget to add `dwmstatus 2>&1 >/dev/null &` to dwm_start (or .xinitrc)
